# Documentación API de Clasificación de Firmas Manuscritas

# Descripción General
Esta API utiliza un modelo de aprendizaje profundo para predecir la personalidad basada en imágenes de firmas manuscritas. El modelo se ha entrenado utilizando la arquitectura InceptionV3 y ha sido implementado usando Python con el framework Flask.

# Tecnologías Utilizadas
Flask: Framework web utilizado para crear la API.
TensorFlow y Keras: Utilizados para cargar y operar el modelo de aprendizaje profundo.
NumPy: Utilizado para manejar las operaciones de matrices.
Pillow: Usado para el procesamiento de imágenes en Python.

# Instalación y Ejecución
Para ejecutar la API, se utiliza un entorno Docker que encapsula todas las dependencias necesarias.

# Comando para ejecutar la API usando Gunicorn
CMD ["gunicorn", "-b", "0.0.0.0:5000", "API:app"]
Este Dockerfile configura un entorno basado en Python 3.7, instala todas las dependencias necesarias, y configura la API para correr en el puerto 5000 usando Gunicorn como servidor WSGI.

# API Endpoints
/predict (POST)
Función: Recibe una imagen de firma manuscrita y devuelve la clase predicha junto con las probabilidades asociadas.
Entrada: Multipart form data con un campo file que contiene la imagen a predecir.
Respuesta: JSON con la clase predicha y un vector de probabilidades.

# Ejemplo de solicitud:
curl -X POST -F "file=@path_to_signature_image.jpg" http://localhost:5000/predict

# Ejemplo de respuesta:
{
  "predicted_class": 2,
  "probabilities": [0.05, 0.03, 0.92]
}

# Seguridad
Autenticación: No implementada actualmente. Para producción, se recomienda añadir autenticación basada en tokens o OAuth para proteger el acceso al API.